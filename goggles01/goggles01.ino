#include <FastLED.h>

#define buttonPin 3
volatile int buttonflag = 0;
volatile int programState = 0;
#define MAXPROGRAMS 10

#define NUM_LEDS 42
#define LOOP 21
#define HALFLOOP 11
#define LAST 41
#define PIN 2
#define MAXBRIGHT 80

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<NEOPIXEL, PIN>(leds, NUM_LEDS);
  pinMode (buttonPin, INPUT);
  //attachInterrupt(1, clickbutton, FALLING);
  Serial.begin(9600);
}

/*
 * Plans:
 * use global buffer array for HSV values that can support transforms
 * 
 */

void loop() {
  for (int i=0; i<=LOOP; i++) {
    leds[i] = CHSV(10, 255, MAXBRIGHT);
    FastLED.delay(100);
    leds[i] = CRGB::Black;
  }
  
  
//  Serial.print("looping with state: ");
//  Serial.println(programState);
//  if (programState == 0) {
//    progressbar(.1);
//  } else if (programState == 1) {
//    glow(.5, 0, 20, MAXBRIGHT);
//  } else if (programState == 2) {
//    crank(0.2, 0);
//  } else if (programState == 3) {
//    portaleyes(0.8);
//  } else if (programState == 4) {
//    rainbowmirror(.5);
//  } else if (programState == 5) {
//    bicolorspin(.5, random8(), 1);
//  } else if (programState == 6) {
//    rainbowinf(.5);
//  } else if (programState == 7) {
//    tricolorspin(.5, random8(), 1);
//  } else if (programState == 8) {
//    tricolorspin(.5, random8(), random8(), random8(), 1);
//  } else if (programState == 9) {
//    demoloop();
//  }
  
  //unittests();
}

void clickbutton() {
  Serial.println("clicked button");
  if (programState < MAXPROGRAMS) {
    programState++;
  } else {
    programState = 0;
  }
}

void demoloop() {
  int count = 5;

  blackout();
  progressbar(.1);
  glow(.5, 0, 20, MAXBRIGHT);
  glow(.5, 0, 20, MAXBRIGHT);
  
  for(int i=0; i<count; i++) {
    portaleyes(0.8);
  }
  for(int i=0; i<count; i++) {
    crank(.2, 0);
  }
  for(int i=0; i<count; i++) {
    rainbowmirror(.5);
  }
  for(int i=0; i<count; i++) {
    rainbowinf(.5);
  }
  for(int i=0; i<count; i++) {
    doubledragons(.5);
  }
  for(int i=0; i<count; i++) {
    bicolorspin(.5, random8(), 1);
    //bicolorspin(1, 20, 90, 1);
  }
  for(int i=0; i<count; i++) {
    tricolorspin(.5, random8(), 1);
    //tricolorspin(1, 0, 160, -1, 1);
  }
  

  for(int i=0; i<count; i++) {
    
  }
}

/* ###################################################################################
 * 
 * SPECIFIC PATTERNS
 * 
 * ###################################################################################
 */



void crank(float frequency, int color) {
  int steps = 7; // not safe to change
  int waitsteps = 4; // safe to change
  int cranks = 8; // safe to change
  //float wait = hztodelay(frequency, (steps + waitsteps) * cranks);
  float wait = hztodelay(1, LOOP);
  static uint8_t hue1 = color;
  static uint8_t hue2 = color;
  int offset = 11;
  int p1, p2, v1, v2;
  for (int c=0; c<cranks; c++) {
    int r = random8(1, 8+1);
    if (r == 1) {
      v1 = 1;
      v2 = 1;
    } else if (r == 2) {
      v1 = -1;
      v2 = -1;
    } else if (r == 3) {
      v1 = 1;
      v2 = -1;
    } else if (r == 4) {
      v1 = -1;
      v2 = 1;
    } else if (r == 5) {
      v1 = 1;
      v2 = 0;
    } else if (r == 6) {
      v1 = -1;
      v2 = 0;
    } else if (r == 7) {
      v1 = 0;
      v2 = 1;
    } else if (r == 8) {
      v1 = 0;
      v2 = -1;
    }
    //hue1 += v1;
    //hue2 += v2;
    CHSV c1(hue1+=v1*5, 255, MAXBRIGHT);
    CHSV c2(hue2+=v2*5, 255, MAXBRIGHT);
    p1 = offset;
    p2 = offset;
    for (int i=0; i<steps; i++) {
      p1 = p1 + v1;
      p2 = p2 + v2;
      writeleft(c1, 3, p1, p1+7, p1+14);
      writeright(c2, 3, p2, p2+7, p2+14);
      FastLED.show();
      FastLED.delay(wait);
      writeleft(CRGB::Black, 3, p1, p1+7, p1+14);
      writeright(CRGB::Black, 3, p2, p2+7, p2+14);
      FastLED.show();
    }
    writeleft(c1, 3, p1, p1+7, p1+14);
    writeright(c2, 3, p2, p2+7, p2+14);
    FastLED.show();
    for (int i=0; i<waitsteps; i++) {
      FastLED.delay(wait);
    }
    writeleft(CRGB::Black, 3, p1, p1+7, p1+14);
    writeright(CRGB::Black, 3, p2, p2+7, p2+14);
    FastLED.show();
  }
}

void doubledragons(float frequency) { 
  float wait = hztodelay(frequency, NUM_LEDS);
  static uint8_t hue1 = 0;
  static uint8_t hue2 = 128;
  for(int i = 0; i < NUM_LEDS; i++) {
    writeinf(CHSV(hue1++, 255, MAXBRIGHT), 1, i);
    writeinf(CHSV(hue2++, 255, MAXBRIGHT), 1, i + LOOP);
    FastLED.show(); 
    dimall(254);
    FastLED.delay(wait);
  }
}

void rainbowinf(float frequency) {
  float wait = hztodelay(frequency, NUM_LEDS);
  static uint8_t hue = 0;
  for(int i = 0; i < NUM_LEDS; i++) {
    writeinf(CHSV(hue++, 255, MAXBRIGHT), 1, i);
    FastLED.show(); 
    dimall(254);
    FastLED.delay(wait);
  }
}

void rainbowmirror(float frequency) {
  float wait = hztodelay(frequency, LOOP);
  for (int i=0; i<LOOP; i++) {
    static uint8_t hue = 0;
    writemirror(CHSV(hue++, 255, MAXBRIGHT), 1, i);
    FastLED.show();
    FastLED.delay(wait);
  }
}

void glow(float frequency, int color, int minbright, int maxbright) {
  float wait = hztodelay(frequency, 2*(maxbright-minbright));
  for (int k=minbright; k<=maxbright; k++) {
    CHSV c(color, 255, k);
    writeall(c);
    FastLED.show();
    FastLED.delay(wait);
  }
  for (int k=maxbright; k>=minbright; k--) {
    CHSV c(color, 255, k);
    writeall(c);
    FastLED.show();
    FastLED.delay(wait);
  }
}

void progressbar(float frequency) {
  float wait = hztodelay(frequency, LOOP);
  for(int i = 0; i < HALFLOOP; i++) {
    writeymirror(CHSV(20+i*4, 255, MAXBRIGHT), 1, i);
    FastLED.show();
    FastLED.delay(wait);
  }
  for(int i = 0; i < HALFLOOP; i++) {
    writeymirror(CHSV(90, 255, MAXBRIGHT), 1, HALFLOOP - 1 - i);
    FastLED.show();
    FastLED.delay(wait/10);
  }
  for(int i = 0; i < LOOP; i++) {
    dimall(200);
    FastLED.delay(wait/10);
  }
}

void portaleyes(float frequency) {
  float wait = hztodelay(frequency, LOOP);
  CHSV Orange(20, 255, MAXBRIGHT);
  CHSV Blue(170, 255, MAXBRIGHT);
  for(int i = 0; i < LOOP; i++) {
    int j = i + HALFLOOP;
    writeleft(Orange, 2, i, j);
    writerightinv(Blue, 2, i, j);
    FastLED.show();
    FastLED.delay(wait);
    dimall(200);
  }
}

void blackout() {
  writeall(CRGB::Black);
}

/*  ###################################################################################
 *  
 *  GENERALIZED PATTERNS
 *  
 *  ###################################################################################
 */

void bicolorspin(float frequency, int color1, int color2, int fade) {
  float wait = hztodelay(frequency, LOOP);
  CRGB c1 = getcolor(color1);
  CRGB c2 = getcolor(color2);
  for (int i=0; i<LOOP; i++) {
    writemirror(c1, 1, i);
    writemirror(c2, 1, i + HALFLOOP);
    FastLED.show();
    FastLED.delay(wait);
    if (fade) {
      dimall(254);
    }
  }
}
void bicolorspin(float frequency, int color, int fade) {
  bicolorspin(frequency, color, color+128, fade);
}

void tricolorspin(float frequency, int color1, int color2, int color3, int fade) {
  float wait = hztodelay(frequency, LOOP);
  CRGB c1 = getcolor(color1);
  CRGB c2 = getcolor(color2);
  CRGB c3 = getcolor(color3);
  for (int i=0; i<LOOP; i++) {
    writemirror(c1, 1, i);
    writemirror(c2, 1, i + 7);
    writemirror(c3, 1, i + 14);
    FastLED.show();
    FastLED.delay(wait);
    if (fade) {
      dimall(254);
    }
  }
}
void tricolorspin(float frequency, int color, int fade) {
  tricolorspin(frequency, color, color+85, color+170, fade);
}

/*  ###################################################################################
 *  
 *  SUPPORT FUNCTIONS
 *  
 *  ###################################################################################
 */

float hztodelay(float cyclespersecond, int loopsize) {
  return 1000 / loopsize / cyclespersecond;
}

void dimall(int scale) {
  for(int i = 0; i < NUM_LEDS; i++) {
    leds[i].nscale8(scale);
  }
}

CRGB getcolor(int hue) {
  if (hue == -1) {
    return CHSV(0, 0, MAXBRIGHT);
  } else {
    return CHSV(hue, 255, MAXBRIGHT);
  }
}

int mod(int a, int b) {
  if (b <= 0) {
    Serial.println("Error, cannot perform % 0");
    return 0;
  }
  if (a == 0) {
    return 0;
  } else if (a > 0) {
    return a%b;
  } else {
    int m = (-a / b) + 1;
    return a + m * b;
  }
}

/* ###################################################################################
 * 
 * STANDARDIZED ADDRESSING FUNCTIONS
 * 
 * ###################################################################################
 */

void write(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), NUM_LEDS);
    leds[a] = color;
  }
  va_end(arguments);
}
void writeinf(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), NUM_LEDS);
    int t;
    if (a < HALFLOOP-1) {
      t = a;
    } else if (a < LOOP) {
      t = a + LOOP;
    } else if (a < LOOP*3/2) {
      t = a;
    } else {
      t = a - LOOP;
    }
    leds[t] = color;
  }
  va_end(arguments);
}
void writeall(CRGB color) {
  for (int i=0; i<NUM_LEDS; i++) {
    leds[i] = color;
  }
}
void writeleft(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), LOOP);
    leds[a] = color;
  }
  va_end(arguments);
}
void writeright(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), LOOP);
    leds[a + LOOP] = color;
  }
  va_end(arguments);
}
void writeleftinv(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), LOOP);
    leds[LOOP - 1 - a] = color;
  }
  va_end(arguments);
}
void writerightinv(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), LOOP);
    leds[NUM_LEDS - 1 - a] = color;
  }
  va_end(arguments);
}
void writeymirror(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), HALFLOOP);
    leds[LOOP/4 + a] = color; // left inside
    leds[(LOOP*5/4 - a) % LOOP] = color; // left outside
    leds[((LOOP*9/4 - a) % LOOP) + LOOP] = color; // right outside
    leds[LOOP*5/4 + a] = color; // right inside
  }
  va_end(arguments);
}
void writexfull(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), LOOP+2);
    if (a <= HALFLOOP-1) {
      leds[a] = color;
      leds[LOOP - a] = color;
    } else {
      leds[LOOP*2 - a] = color;
      leds[LOOP + a] = color;
    }
  }
  va_end(arguments);
}
void writexmirror(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), HALFLOOP);
    leds[a] = color;
    leds[LOOP - a] = color;
    leds[LOOP*2 - a] = color;
    leds[LOOP + a] = color;
  }
  va_end(arguments);
}
void writexcopy(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), HALFLOOP);
    leds[a] = color; // left bottom
    leds[LOOP - a] = color; // left top
    leds[((LOOP*3/2 - a) % LOOP) + LOOP] = color; // right bottom
    leds[LOOP*3/2 + a] = color; // right top
  }
  va_end(arguments);
}
void writemirror(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x = 0; x < numarg; x++ ) {
    int a = mod(va_arg(arguments, int), LOOP);
    leds[a] = color; // left
    leds[a + LOOP] = color; // right
  }
  va_end(arguments);
}
void writecopy(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x = 0; x < numarg; x++ ) {
    int a = mod(va_arg(arguments, int), LOOP);
    leds[a] = color; // left
    leds[((LOOP*3/2 - a) % LOOP) + LOOP] = color; // right
  }
  va_end(arguments);
}

/* ###################################################################################
 * 
 * ADDRESSING UNIT TEST FUNCTIONS
 * 
 * ###################################################################################
 */

void unittests() {
  test();
  testinf();
  testmirror();
  testcopy();
  testxfull();
  testxmirror();
  testxcopy();
  testymirror();
}

void test() {
  for (int i=0; i<NUM_LEDS; i++) {
    write(CRGB::Red, 1, i);
    FastLED.delay(100);
    write(CRGB::Black, 1, i);
  }
}
void testinf() {
  for (int i=0; i<NUM_LEDS; i++) {
    writeinf(CRGB::Red, 1, i);
    FastLED.delay(100);
    writeinf(CRGB::Black, 1, i);
  }
}
void testmirror() {
  for (int i=0; i<LOOP; i++) {
    writemirror(CRGB::Red, 1, i);
    FastLED.delay(100);
    writemirror(CRGB::Black, 1, i);
  }
}
void testcopy() {
  for (int i=0; i<LOOP; i++) {
    writecopy(CRGB::Red, 1, i);
    FastLED.delay(100);
    writecopy(CRGB::Black, 1, i);
  }
}
void testxfull() {
  for (int i=0; i<LOOP; i++) {
    writexfull(CRGB::Red, 1, i);
    FastLED.delay(100);
    writexfull(CRGB::Black, 1, i);
  }
}
void testxmirror() {
  for (int i=0; i<HALFLOOP; i++) {
    writexmirror(CRGB::Red, 1, i);
    FastLED.delay(100);
    writexmirror(CRGB::Black, 1, i);
  }
}
void testxcopy() {
  for (int i=0; i<HALFLOOP; i++) {
    writexcopy(CRGB::Red, 1, i);
    FastLED.delay(100);
    writexcopy(CRGB::Black, 1, i);
  }
}
void testymirror() {
  for (int i=0; i<HALFLOOP; i++) {
    writeymirror(CRGB::Red, 1, i);
    FastLED.delay(100);
    writeymirror(CRGB::Black, 1, i);
  }
}
