#include <FastLED.h>

//####################################################################################
//                                  DEFINITIONS
//####################################################################################

#define LEFT 0
#define RIGHT 1
#define LOOP 21
#define HALFLOOP 11
#define PI 3.141592654
#define TWOPI 6.283185307
#define MAXBRIGHT 80

#define RIGHTPIN 2
#define LEFTPIN 3

//####################################################################################
//                                  DATA ARRAY
//####################################################################################

CRGB leds[2][LOOP];

//####################################################################################
//                                  GLOBAL COLORS
//####################################################################################

CRGB b = CRGB::Black;
CRGB c = CHSV(20, 255, MAXBRIGHT);
CRGB d = CHSV(140, 255, MAXBRIGHT);

//####################################################################################
//                                  SETUP
//####################################################################################

void setup() {
  FastLED.addLeds<NEOPIXEL, LEFTPIN>(leds[LEFT], LOOP);
  FastLED.addLeds<NEOPIXEL, RIGHTPIN>(leds[RIGHT], LOOP);
  Serial.begin(9600);
  Serial.println("Beginning goggle02");
}

//####################################################################################
//                                  MAIN LOOP
//####################################################################################

void loop() {
  testwritex();
  testwritey();
//  testwritetheta();
//  testwritereal();
//  portaleyes(0.8);
//  glow(.5, CHSV(0, 255, MAXBRIGHT), 20, MAXBRIGHT);
//    crank(0.2, 0);
}

//####################################################################################
//                                  ROUTINES
//####################################################################################

void glow(float frequency, CRGB color, int minbright, int maxbright) {
  float wait = hztodelay(frequency, 2*(maxbright-minbright));
  int factor = 255 / maxbright;
  for (int k=minbright; k<=maxbright; k++) {
    writeall(color);
    dimall(k*factor);
    FastLED.show();
    FastLED.delay(wait);
  }
  for (int k=maxbright; k>=minbright; k--) {
    writeall(color);
    dimall(k*factor);
    FastLED.show();
    FastLED.delay(wait);
  }
}

void portaleyes(float frequency) {
  float wait = hztodelay(frequency, LOOP);
  CHSV Orange(20, 255, MAXBRIGHT);
  CHSV Blue(170, 255, MAXBRIGHT);
  for(int i = 0; i < LOOP; i++) {
    int j = w(i + HALFLOOP);
    leds[LEFT][i] = Orange;
    leds[LEFT][j] = Orange;
    leds[RIGHT][w(-i)] = Blue;
    leds[RIGHT][w(-j)] = Blue;
    FastLED.show();
    FastLED.delay(wait);
    dimall(200);
  }
}

void crank(float frequency, int color) {
  int steps = 7; // not safe to change
  int waitsteps = 4; // safe to change
  int cranks = 8; // safe to change
  //float wait = hztodelay(frequency, (steps + waitsteps) * cranks);
  float wait = hztodelay(1, LOOP);
  static uint8_t hue1 = color;
  static uint8_t hue2 = color;
  int offset = 11;
  int p1, p2, v1, v2;
  for (int c=0; c<cranks; c++) {
    int r = random8(1, 8+1);
    if (r == 1) {
      v1 = 1;
      v2 = 1;
    } else if (r == 2) {
      v1 = -1;
      v2 = -1;
    } else if (r == 3) {
      v1 = 1;
      v2 = -1;
    } else if (r == 4) {
      v1 = -1;
      v2 = 1;
    } else if (r == 5) {
      v1 = 1;
      v2 = 0;
    } else if (r == 6) {
      v1 = -1;
      v2 = 0;
    } else if (r == 7) {
      v1 = 0;
      v2 = 1;
    } else if (r == 8) {
      v1 = 0;
      v2 = -1;
    }
    //hue1 += v1;
    //hue2 += v2;
    CHSV c1(hue1+=v1*5, 255, MAXBRIGHT);
    CHSV c2(hue2+=v2*5, 255, MAXBRIGHT);
    p1 = offset;
    p2 = offset;
    for (int i=0; i<steps; i++) {
      p1 = p1 + v1;
      p2 = p2 + v2;
      writeleft(c1, 3, p1, p1+7, p1+14);
      writeright(c2, 3, p2, p2+7, p2+14);
      FastLED.show();
      FastLED.delay(wait);
      writeleft(CRGB::Black, 3, p1, p1+7, p1+14);
      writeright(CRGB::Black, 3, p2, p2+7, p2+14);
      FastLED.show();
    }
    writeleft(c1, 3, p1, p1+7, p1+14);
    writeright(c2, 3, p2, p2+7, p2+14);
    FastLED.show();
    for (int i=0; i<waitsteps; i++) {
      FastLED.delay(wait);
    }
    writeleft(CRGB::Black, 3, p1, p1+7, p1+14);
    writeright(CRGB::Black, 3, p2, p2+7, p2+14);
    FastLED.show();
  }
}

//####################################################################################
//                                  SUPPORT FUNCTIONS
//####################################################################################

// Sets all leds to black
void blackout() {
  writeall(CRGB::Black);
  FastLED.show();
}
void blackout(int strip) {
  writeall(CRGB::Black, strip);
  FastLED.show();
}

// Lowers the brightness of all leds
void dimall(int scale) {
  for(int i = 0; i < LOOP; i++) {
    leds[LEFT][i].nscale8(scale);
    leds[RIGHT][i].nscale8(scale);
  }
}
void dimall(int scale, int strip) {
  for(int i = 0; i < LOOP; i++) {
    leds[strip][i].nscale8(scale);
  }
}

//####################################################################################
//                                  MATH FUNCTIONS
//####################################################################################

float hztodelay(float cyclespersecond, int loopsize) {
  return 1000 / loopsize / cyclespersecond;
}

// Returns x mod y, works for negative x's
int mod(int a, int b){
  if (b <= 0) {
    Serial.println("Error, cannot perform % < 0");
    return 0;
  }
  if (a == 0) {
    return 0;
  } else if (a > 0) {
    return a%b;
  } else {
    int m = (-a / b) + 1;
    return a + m * b;
  }
}

// Wraps a number to output an int between 0 and LOOP
int w(int x) {
  return mod(x, LOOP);
}

// Wraps a number to output a float between 0 and LOOP
float wf(float x) {
  while (x < LOOP) {
    x += LOOP;
  }
  while (x >= LOOP) {
    x-= LOOP;
  }
  return x;
}

//####################################################################################
//                                  WRITE FUNCTIONS
//####################################################################################

// Writes to all leds
void writeall(CRGB color) {
  for (int i=0; i<LOOP; i++) {
    leds[LEFT][i] = color;
    leds[RIGHT][i] = color;
  }
}
void writeall(CRGB color, int strip) {
  for (int i=0; i<LOOP; i++) {
    leds[strip][i] = color;
  }
}

void writeleft(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), LOOP);
    leds[LEFT][a] = color;
  }
  va_end(arguments);
}
void writeright(CRGB color, int numarg, ...) {
// sets any number of leds to the specified color
// ex. write(CRGB::Red, 5, 2, 4, 6, 8, 10);
  va_list arguments;
  va_start(arguments, numarg);
  for ( int x=0; x<numarg; x++ ) {
    int a = mod(va_arg(arguments, int), LOOP);
    leds[RIGHT][a] = color;
  }
  va_end(arguments);
}

// Writes to a real numbered position, using linear interpolation
void writereal(CRGB color, int strip, float real) {
  real = wf(real);
  int whole = (int) real;
  float remainder = real - whole;
  int nextwhole = w(whole + 1);
  int r = color.red;
  int g = color.green;
  int b = color.blue;
  leds[strip][whole] = CRGB(r * (1 - remainder), g * (1 - remainder), b * (1 - remainder));
  leds[strip][nextwhole] = CRGB(r * remainder, g * remainder, b * remainder);
}

// Writes along the x direction
void writex(CRGB color, int strip, int pos) {
  leds[strip][pos] = color;
  leds[strip][w(LOOP - pos)] = color;
}

// Writes along the y direction
void writey(CRGB color, int strip, int pos) {
  leds[strip][w(pos - LOOP/4)] = color;
  leds[strip][w(LOOP*3/4 - pos)] = color;
}

// Writes a theta position
void writetheta(CRGB color, int strip, float pos) {
  writereal(color, strip, pos * LOOP / TWOPI);
}

//####################################################################################
//                                  TEST FUNCTIONS
//####################################################################################

void testwritereal() {
  for (float i=0; i < LOOP * 10; i++) {
    writereal(c, LEFT, i / 10);
    leds[RIGHT][(int) i/10] = d;
    FastLED.delay(50);
    blackout();
  }
}

void testwritex() {
  for (int i=0; i<HALFLOOP; i++) {
    writex(c, LEFT, i);
    writex(d, RIGHT, HALFLOOP-i-1);
    FastLED.delay(100);
    writex(b, LEFT, i);
    writex(b, RIGHT, HALFLOOP-i-1);
  }
}

void testwritey() {
  for (int i=0; i<HALFLOOP; i++) {
    writey(c, LEFT, i);
    writey(d, RIGHT, HALFLOOP-i-1);
    FastLED.delay(100);
    writey(b, LEFT, i);
    writey(b, RIGHT, HALFLOOP-i-1);
  }
}

void testwritetheta() {
  float stepsize = 0.1;
  float i = 0;
  while (i < TWOPI) {
    writetheta(c, LEFT, i);
    writetheta(d, RIGHT, -i);
    FastLED.delay(30);
    writetheta(b, LEFT, i);
    writetheta(b, RIGHT, -i);
    i += stepsize;
  }
}
